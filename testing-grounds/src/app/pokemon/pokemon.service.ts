import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ALL, POKEMON_URI, pokemonList } from "./pokemon.definition";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor( private http: HttpClient) { }


  getPokemon(next: boolean, nextUrl: string): Observable<pokemonList> {
      return this.http.get<pokemonList>(next? nextUrl : POKEMON_URI);
  }
}
