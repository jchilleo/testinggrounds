import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {PokemonService} from "./pokemon.service";
import {FormControl} from '@angular/forms';
import {results, pokemonData, pokemonType, iPokemonMapping} from './pokemon.definition';
import {MatSelect,} from "@angular/material/select";

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit, AfterViewInit {

  constructor(protected pokemonService: PokemonService) {
  }

  // @ts-ignore
  @ViewChild('pokemonSelect') pokemonScroll: MatSelect;

  infiniteScrollLocation = 200;

  disableSelect = new FormControl(false);
  pokemon = {
    count: 0,
    data: {} as pokemonData,
    loaded: {
      data: false,
      list: false
    },
    list: Array<results>(),
    next: '',
    selected: {
      name: '',
      url: ''
    }
  };

  ngOnInit(): void {
    this.getPokemonList();

  }

  ngAfterViewInit(): void {
    this.pokemonScroll._openedStream.subscribe(() =>
      this.registerPanelScrollEvent());
  }


  //region misc

  loadNextSetOfPokemon(event: any) {
    if (event.target.scrollTop > this.infiniteScrollLocation && (this.pokemon.next !== null || this.pokemon.list.length < this.pokemon.count)) {
      this.infiniteScrollLocation += 500;
      this.getPokemonList(true, this.pokemon.next)
    }
  }

  registerPanelScrollEvent() {
    const pokemonSelectScrollBar = this.pokemonScroll.panel.nativeElement;
    pokemonSelectScrollBar.addEventListener('scroll', (event: any) => this.loadNextSetOfPokemon(event))
  }

  //endregion misc

  //region getters
  getPokemonData(): void {
    this.pokemonService.getPokemon(true, this.pokemon.selected.url).subscribe(data => {

      // @ts-ignore
      this.pokemon.data = data;

      // @ts-ignore
      this.pokemon.next = data.next;
      this.pokemon.loaded.data = true;
    })
  }

  getPokemonList(next: boolean = false, nextUrl: string = '') {
    this.pokemonService.getPokemon(next, nextUrl).subscribe(data => {

      if (next) {
        // @ts-ignore
        this.pokemon.list = this.pokemon.list.concat(data.results);
      } else {
        // @ts-ignore
        this.pokemon.list = data.results;
        // @ts-ignore
        this.pokemon.count = data.count;
      }
      // @ts-ignore
      this.pokemon.next = data.next;
      this.pokemon.loaded.list = true;
    })
  }

  //endregion getters

  //region data-manipulation

  extractPokemonTypes(): string {
    let types = ''
    for(let pType of this.pokemon.data.types) {
      types = (types.length > 0) ? types + ', ' + pType.type.name : pType.type.name;
    }
    return types;
  }

  //endregion data-manipulation

}
