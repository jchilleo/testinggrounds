export const POKEMON_URI = 'https://pokeapi.co/api/v2/pokemon/';
export const ALL = '?offset=20&limit=20';

export interface iPokemonMapping {
  count: number | string,
  next: string | null,
  previous: string | null,
  results: Array<results>
}

export interface results {
  name?: string,
  url?: string
}

export type pokemonList = Array<iPokemonMapping>;


export interface pokemonData {
  abilities: Array<ability>,
  base_experience: number | string,
  forms: Array<pokemonForm>,
  game_indices: Array<game_index>,
  height: number | string,
  held_items: Array<any>,
  id: number | string,
  is_default: boolean,
  location_area_encounters: string,
  moves: Array<pokemonMove>,
  name: string,
  order: number | string,
  species: {
    name: string,
    url: string
  },
  sprites: {
    back_default: string | null,
    back_female: string | null,
    back_shiny: string | null,
    back_shiny_female: string | null,
    front_default: string | null,
    front_female: string | null,
    front_shiny: string | null,
    front_shiny_female: string | null,
    other: {},
    versions: {},
  },
  stats: Array<stat>,
  types: Array<pokemonType>,
  weight: number | string


}

interface ability {
  ability: {
    name: string,
    url: string
  },
  is_hidden: boolean,
  slot: number | string
}

interface pokemonForm {
  name: string,
  url: string
}

interface game_index {
  game_index: number | string,
  version: {
    name: string,
    url: string
  }
}

interface pokemonMove {
  move: {
    name: string,
    url: string
  },
  version_group_details: Array<version_group_detail>
}

interface version_group_detail {
  level_learned_at: number | string,
  move_learn_method: {
    name: string,
    url: string
  },
  version_group: {
    name: string,
    url: string
  }
}

interface stat {
  base_stat: number | string,
  effort: number | string,
  stat: {
    name: string,
    url: string
  }
}

export interface pokemonType {
  slot: number | string,
  type: {
    name: string,
    url: string
  }
}
